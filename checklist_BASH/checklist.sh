#!/bin/bash

log="item.csv"
echo "Welcome to the check list"

echo "Please enter your name: "
name="$(cat name.lst|fzf)"

echo "Welcome $name"

cat item.lst|while read item
do
  d="$(date)"
  t="$(date +%s)"
  echo "$item"
  read i </dev/tty

  if [ "$i" = "n" ];then
    i="Problem"
    echo "$t,$d,$name,$item,$i" >> $log
  elif [ "$i" = "na" ];then
    i="N/A"
    echo "$t,$d,$name,$item,$i" >> $log
  else
    i="GOOD"
    echo "$t,$d,$name,$item,$i" >> $log
  fi

done
